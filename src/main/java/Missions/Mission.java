package Missions;

import fly.Flyable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import util.Coordinates;

@AllArgsConstructor
@Getter
public abstract class Mission {
  Coordinates destination;
  String pilotName;
  Flyable flyable;

  protected abstract String executeMission();

  public void begin() {
    System.out.println("Beginning Mission!");
    this.flyable.flyTo(this.destination);
  }

  public void cancel() {
    System.out.println("Abort Mission!");
    this.flyable.land(this.flyable.getMotherBase());
  }

  public void finish() {
    System.out.println(this.executeMission());
    this.flyable.land(this.flyable.getMotherBase());
    System.out.println("Finish Mission!");
  }
}
