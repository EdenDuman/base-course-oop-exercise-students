package Missions;

import fly.Attack;
import lombok.Getter;
import util.Coordinates;

@Getter
public class AttackMission extends Mission {
  String target;

  public AttackMission(Coordinates destination, String pilotName, Attack flyable, String target) {
    super(destination, pilotName, flyable);
    this.target = target;
  }

  @Override
  protected String executeMission() {
    Attack attack = (Attack) this.flyable;

    return String.format(
        "%s:%s Attacking %s with: %sX%d",
        this.pilotName,
        this.flyable.getAircraftType().name(),
        this.target,
        attack.getMissileType().name(),
        attack.getAmountOfMissiles());
  }
}
