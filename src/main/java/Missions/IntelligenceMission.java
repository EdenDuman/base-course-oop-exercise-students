package Missions;

import fly.Intelligence;
import lombok.Getter;
import util.Coordinates;

@Getter
public class IntelligenceMission extends Mission {
  String region;

  public IntelligenceMission(
      Coordinates destination, String pilotName, Intelligence flyable, String region) {
    super(destination, pilotName, flyable);
    this.region = region;
  }

  @Override
  protected String executeMission() {
    Intelligence intelligence = (Intelligence) this.flyable;

    return String.format(
        "%s:%s Collecting Data in %s with: %s",
        this.pilotName,
        this.flyable.getAircraftType().name(),
        this.region,
        intelligence.getSensorType().name());
  }
}
