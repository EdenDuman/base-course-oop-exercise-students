package Missions;

import fly.Bda;
import lombok.Getter;
import util.Coordinates;

@Getter
public class BdaMission extends Mission {
  String objective;

  public BdaMission(Coordinates destination, String pilotName, Bda flyable,
      String objective) {
    super(destination, pilotName, flyable);
    this.objective = objective;
  }

  @Override
  protected String executeMission() {
    Bda bda = (Bda) this.flyable;

    return String.format(
        "%s:%s taking pictures of %s with: %s",
        this.pilotName,
        this.flyable.getAircraftType().name(),
        this.objective,
        bda.getCameraType().name());
  }
}
