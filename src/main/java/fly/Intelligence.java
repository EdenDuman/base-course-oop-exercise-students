package fly;

import enums.SensorType;

public interface Intelligence extends Flyable {
  SensorType getSensorType();

  void setSensorType(SensorType sensorType);
}
