package fly;

import enums.CameraType;

public interface Bda extends Flyable {
  CameraType getCameraType();

  void setCameraType(CameraType cameraType);
}
