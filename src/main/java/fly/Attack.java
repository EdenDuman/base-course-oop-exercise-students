package fly;

import enums.MissileType;

public interface Attack extends Flyable {
  Integer getAmountOfMissiles();

  MissileType getMissileType();

  void setAmountOfMissiles(Integer amountOfMissiles);

  void setMissileType(MissileType missileType);
}
