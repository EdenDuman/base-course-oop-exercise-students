package fly;

import enums.AircraftType;
import util.Coordinates;

public interface Flyable {
  void flyTo(Coordinates destination);

  void land(Coordinates destination);

  Coordinates getMotherBase();

  AircraftType getAircraftType();

  void setMotherBase(Coordinates coordinates);
}
