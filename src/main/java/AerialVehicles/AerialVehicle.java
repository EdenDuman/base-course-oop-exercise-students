package AerialVehicles;

import enums.FlightStatus;
import fly.Flyable;
import lombok.Getter;
import lombok.Setter;
import util.Coordinates;

@Getter
@Setter
public abstract class AerialVehicle implements Flyable {
  private final Integer limitFlightHours;

  protected Integer flightHoursSinceTheLastRepair;
  protected FlightStatus flightStatus;
  protected Coordinates motherBase;

  public AerialVehicle(
      Integer limitFlightHours, FlightStatus flightStatus, Coordinates motherBase) {
    this.limitFlightHours = limitFlightHours;
    this.flightHoursSinceTheLastRepair = 0;
    this.flightStatus = flightStatus;
    this.motherBase = motherBase;
  }

  @Override
  public void flyTo(Coordinates destination) {
    this.flightStatus = this.flightStatus.fly(destination);
  }

  @Override
  public void land(Coordinates destination) {
    if (!this.flightStatus.equals(FlightStatus.IN_THE_AIR)) {
      System.out.println("Aerial Vehicle is already on the ground");
    } else {
      System.out.println("Landing on: " + destination.toString());
      this.motherBase = destination;
      this.check();
    }
  }

  private void check() {
    if (this.flightHoursSinceTheLastRepair >= this.limitFlightHours) {
      this.flightStatus = FlightStatus.NOT_READY;
      this.repair();
    } else {
      this.flightStatus = FlightStatus.READY;
    }
  }

  private void repair() {
    this.flightHoursSinceTheLastRepair = 0;
    this.flightStatus = FlightStatus.READY;
  }
}
