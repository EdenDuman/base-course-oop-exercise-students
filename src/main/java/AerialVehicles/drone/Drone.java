package AerialVehicles.drone;

import AerialVehicles.AerialVehicle;
import enums.FlightStatus;
import lombok.Getter;
import util.Coordinates;

@Getter
public abstract class Drone extends AerialVehicle {
  public Drone(Integer limitFlightHours, FlightStatus flightStatus, Coordinates motherBase) {
    super(limitFlightHours, flightStatus, motherBase);
  }

  public void hoverOverLocation(Coordinates destination) {
    if (!this.flightStatus.equals(FlightStatus.IN_THE_AIR)) {
      System.out.println("Aerial Vehicle is on the ground");
    } else {
      System.out.println("Hovering Over:" + destination.toString());
    }
  }
}
