package AerialVehicles.drone.haron;

import AerialVehicles.drone.Drone;
import enums.FlightStatus;
import lombok.Getter;
import lombok.Setter;
import util.Constant;
import util.Coordinates;

@Getter
@Setter
public abstract class Haron extends Drone {
  public Haron(FlightStatus flightStatus, Coordinates motherBase) {
    super(Constant.HARON_LIMIT_FLIGHT_HOURS, flightStatus, motherBase);
  }
}
