package AerialVehicles.drone.haron;

import com.google.inject.Inject;
import enums.AircraftType;
import enums.FlightStatus;
import enums.MissileType;
import enums.SensorType;
import fly.Attack;
import fly.Intelligence;
import lombok.Getter;
import lombok.Setter;
import util.Coordinates;

@Getter
@Setter
public class Eitan extends Haron implements Intelligence, Attack {
  private final AircraftType aircraftType = AircraftType.EITAN;

  private Integer amountOfMissiles;
  private MissileType missileType;
  private SensorType sensorType;

  @Inject
  public Eitan(
      FlightStatus flightStatus,
      Coordinates motherBase,
      Integer amountOfMissiles,
      MissileType missileType,
      SensorType sensorType) {
    super(flightStatus, motherBase);
    this.amountOfMissiles = amountOfMissiles;
    this.missileType = missileType;
    this.sensorType = sensorType;
  }
}
