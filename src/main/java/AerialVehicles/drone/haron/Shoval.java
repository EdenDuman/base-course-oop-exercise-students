package AerialVehicles.drone.haron;

import com.google.inject.Inject;
import enums.AircraftType;
import enums.CameraType;
import enums.FlightStatus;
import enums.MissileType;
import enums.SensorType;
import fly.Attack;
import fly.Bda;
import fly.Intelligence;
import lombok.Getter;
import lombok.Setter;
import util.Coordinates;

@Getter
@Setter
public class Shoval extends Haron implements Intelligence, Bda, Attack {
  private final AircraftType aircraftType = AircraftType.SHOVAL;

  private Integer amountOfMissiles;
  private MissileType missileType;
  private CameraType cameraType;
  private SensorType sensorType;

  @Inject
  public Shoval(
      FlightStatus flightStatus,
      Coordinates motherBase,
      Integer amountOfMissiles,
      MissileType missileType,
      CameraType cameraType,
      SensorType sensorType) {
    super(flightStatus, motherBase);
    this.amountOfMissiles = amountOfMissiles;
    this.missileType = missileType;
    this.cameraType = cameraType;
    this.sensorType = sensorType;
  }
}
