package AerialVehicles.drone.hermes;

import com.google.inject.Inject;
import enums.AircraftType;
import enums.CameraType;
import enums.FlightStatus;
import enums.SensorType;
import fly.Bda;
import fly.Intelligence;
import lombok.Getter;
import lombok.Setter;
import util.Coordinates;

@Getter
@Setter
public class Zik extends Hermes implements Intelligence, Bda {
  private final AircraftType aircraftType = AircraftType.ZIK;

  private CameraType cameraType;
  private SensorType sensorType;

  @Inject
  public Zik(
      FlightStatus flightStatus,
      Coordinates motherBase,
      CameraType cameraType,
      SensorType sensorType) {
    super(flightStatus, motherBase);
    this.cameraType = cameraType;
    this.sensorType = sensorType;
  }
}
