package AerialVehicles.drone.hermes;

import AerialVehicles.drone.Drone;
import enums.FlightStatus;
import lombok.Getter;
import lombok.Setter;
import util.Constant;
import util.Coordinates;

@Getter
@Setter
public abstract class Hermes extends Drone {
  public Hermes(FlightStatus flightStatus, Coordinates motherBase) {
    super(Constant.HERMES_LIMIT_FLIGHT_HOURS, flightStatus, motherBase);
  }
}
