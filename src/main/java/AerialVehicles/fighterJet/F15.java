package AerialVehicles.fighterJet;

import com.google.inject.Inject;
import enums.AircraftType;
import enums.FlightStatus;
import enums.MissileType;
import enums.SensorType;
import fly.Attack;
import fly.Intelligence;
import lombok.Getter;
import lombok.Setter;
import util.Coordinates;

@Getter
@Setter
public class F15 extends FighterJets implements Attack, Intelligence {
  private final AircraftType aircraftType = AircraftType.F15;

  private Integer amountOfMissiles;
  private MissileType missileType;
  private SensorType sensorType;

  @Inject
  public F15(
      FlightStatus flightStatus,
      Coordinates motherBase,
      Integer amountOfMissiles,
      MissileType missileType,
      SensorType sensorType) {
    super(flightStatus, motherBase);
    this.amountOfMissiles = amountOfMissiles;
    this.missileType = missileType;
    this.sensorType = sensorType;
  }
}
