package AerialVehicles.fighterJet;

import com.google.inject.Inject;
import enums.AircraftType;
import enums.CameraType;
import enums.FlightStatus;
import enums.MissileType;
import fly.Attack;
import fly.Bda;
import lombok.Getter;
import lombok.Setter;
import util.Coordinates;

@Getter
@Setter
public class F16 extends FighterJets implements Attack, Bda {
  private final AircraftType aircraftType = AircraftType.F16;

  private Integer amountOfMissiles;
  private MissileType missileType;
  private CameraType cameraType;

  @Inject
  public F16(
      FlightStatus flightStatus,
      Coordinates motherBase,
      Integer amountOfMissiles,
      MissileType missileType,
      CameraType cameraType) {
    super(flightStatus, motherBase);
    this.amountOfMissiles = amountOfMissiles;
    this.missileType = missileType;
    this.cameraType = cameraType;
  }
}
