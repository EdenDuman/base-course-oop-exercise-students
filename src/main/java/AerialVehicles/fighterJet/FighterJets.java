package AerialVehicles.fighterJet;

import AerialVehicles.AerialVehicle;
import enums.FlightStatus;
import lombok.Getter;
import lombok.Setter;
import util.Constant;
import util.Coordinates;

@Getter
@Setter
public abstract class FighterJets extends AerialVehicle {
  public FighterJets(FlightStatus flightStatus, Coordinates motherBase) {
    super(Constant.FIGHTER_JET_LIMIT_FLIGHT_HOURS, flightStatus, motherBase);
  }
}
