import util.RunMissions;

public class Main {
  public static void main(String[] args) {
    RunMissions runMissions = new RunMissions();

    runMissions.intelligenceMission();
    runMissions.attackMission();
    runMissions.bdaMission();
  }
}
