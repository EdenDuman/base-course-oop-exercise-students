package util;

import AerialVehicles.drone.Drone;
import AerialVehicles.drone.haron.Eitan;
import AerialVehicles.fighterJet.F15;
import AerialVehicles.fighterJet.F16;
import AerialVehicles.fighterJet.FighterJets;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;
import Missions.Mission;
import enums.CameraType;
import enums.FlightStatus;
import enums.MissileType;
import enums.SensorType;
import fly.Attack;
import fly.Bda;
import fly.Intelligence;

public class RunMissions {
  private void printLine() {
    System.out.println("-------------------------------------------------");
  }

  private void startMission(String missionName) {
    printLine();
    System.out.println("start " + missionName + " mission");
    System.out.println();
  }

  private void endMission() {
    printLine();
    System.out.println();
    System.out.println();
  }

  public void intelligenceMission() {
    Coordinates homeBase = new Coordinates(31.827604665263365, 34.81739714569337);
    Drone drone =
        new Eitan(FlightStatus.READY, homeBase, 4, MissileType.PYTHON, SensorType.INFRA_RED);

    Coordinates destination = new Coordinates(33.2033427805222, 44.5176910795946);
    Mission mission =
        new IntelligenceMission(destination, "Dror zalicha", (Intelligence) drone, "Iraq");

    this.startMission("intelligence");
    mission.begin();
    ((Drone) mission.getFlyable()).hoverOverLocation(mission.getDestination());
    mission.finish();
    this.endMission();
  }

  public void attackMission() {
    Coordinates homeBase = new Coordinates(31.827604665263365, 34.81739714569337);
    FighterJets fighterJets =
        new F15(FlightStatus.READY, homeBase, 250, MissileType.SPICE250, SensorType.INFRA_RED);

    Coordinates destination = new Coordinates(33.2033427805222, 44.5176910795946);
    Mission mission =
        new AttackMission(
            destination, "Ze'ev Raz", (Attack) fighterJets, "Tuwaitha Nuclear Research Center");

    this.startMission("attack");
    mission.begin();
    mission.finish();
    this.endMission();
  }

  public void bdaMission() {
    Coordinates homeBase = new Coordinates(31.827604665263365, 34.81739714569337);
    FighterJets fighterJets =
        new F16(FlightStatus.READY, homeBase, 250, MissileType.SPICE250, CameraType.THERMAL);

    Coordinates destination = new Coordinates(33.2033427805222, 44.5176910795946);
    Mission mission =
        new BdaMission(
            destination, "Ilan Ramon", (Bda) fighterJets, "Tuwaitha Nuclear Research Center");

    this.startMission("Bda");
    mission.begin();
    mission.finish();
    this.endMission();
  }
}
