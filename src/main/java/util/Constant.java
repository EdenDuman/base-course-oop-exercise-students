package util;

public class Constant {
  public static final Integer FIGHTER_JET_LIMIT_FLIGHT_HOURS = 250;
  public static final Integer HARON_LIMIT_FLIGHT_HOURS = 150;
  public static final Integer HERMES_LIMIT_FLIGHT_HOURS = 100;
}
