package util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class Coordinates {
  private Double longitude;
  private Double latitude;

  @Override
  public String toString() {
    return this.longitude + ":" + this.latitude;
  }
}
