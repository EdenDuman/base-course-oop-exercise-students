package enums;

import util.Coordinates;

public enum FlightStatus {
  READY {
    @Override
    public FlightStatus fly(Coordinates destination) {
      System.out.println("Flying to: " + destination.toString());
      return FlightStatus.IN_THE_AIR;
    }
  },
  NOT_READY {
    @Override
    public FlightStatus fly(Coordinates destination) {
      System.out.println("Aerial Vehicle isn't ready to fly");
      return this;
    }
  },
  IN_THE_AIR {
    @Override
    public FlightStatus fly(Coordinates destination) {
      System.out.println("Aerial Vehicle already in air");
      return this;
    }
  };

  public abstract FlightStatus fly(Coordinates destination);
}
