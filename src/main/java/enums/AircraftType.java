package enums;

public enum AircraftType {
  F16,
  F15,
  ZIK,
  KACHAV,
  SHOVAL,
  EITAN
}
