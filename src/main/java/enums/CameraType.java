package enums;

public enum CameraType {
  REGULAR,
  THERMAL,
  NIGHT_VISION
}
